//
//  HistoryResultsViewController.m
//  findMyRep
//
//  Created by Joal Arcos on 5/30/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import "HistoryResultsViewController.h"
#import "HistoryViewController.h"

@interface HistoryResultsViewController ()
@property (nonatomic) HistoryViewController *hvc;
@end

@implementation HistoryResultsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Return the number of sections.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of rows in the section.
	return 5;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
	
	// Configure the cell...
	cell.textLabel.text = @"Hello";
	cell.detailTextLabel.text = @"MR";
	
	return cell;
}

#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	_hvc = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController" bundle:nil];
	[self presentViewController:_hvc animated:YES completion:nil];
}

- (IBAction)dismissTableView:(id)sender
{
	[_delegate removeTableView];
}


@end
