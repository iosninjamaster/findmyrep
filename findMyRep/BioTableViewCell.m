//
//  BioTableViewCell.m
//  findMyRep
//
//  Created by Joal Arcos on 4/28/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import "BioTableViewCell.h"

@implementation BioTableViewCell

- (void)awakeFromNib
{
    //Set button text to be left aligned
    self.link.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/**
 *  This is called when the user taps on a link on the custom Cell. It pushes the WebView and loads the URL.
 */
- (void)sendLink:(id)sender
{
    UIButton *button = (UIButton *)sender;
    WebViewViewController *viewController = [WebViewViewController new];
    viewController.selectedURL = button.titleLabel.text;
    [self.navi pushViewController:viewController animated:YES];
}
@end
