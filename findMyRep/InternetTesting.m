//
//  InternetTesting.m
//  findMyRep
//
//  Created by Joal Arcos on 4/30/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import "InternetTesting.h"
static float const kMaximumElapsedTime = 2.0;

@implementation InternetTesting

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    self.startTime = [NSDate date];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to the instance variable you declared
    self.length += [data length];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse
{
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    //self.connection = nil;
    self.connection = nil;
    [_delegate updateConnectionSpeed:[self determineMegabytesPerSecond]];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // The request has failed for some reason!
    // Check the error var
    if (self.connection)
    {
        self.connection = nil;
    }
    if (error)
    {
        //If there was an error with the internet connection, send the error to the HomepageVC
        [_delegate noInternetError:error];
    }

}

/**
 *  Used to calculate the MB/sec
 */
- (float)determineMegabytesPerSecond
{
    NSTimeInterval elapsed;
    
    if (self.startTime)
    {
        elapsed = [[NSDate date] timeIntervalSinceDate:self.startTime];
        return self.length / elapsed / 1024 / 1024;
    }
    return -1;
}

/**
 *  This is the first method called in this class. It is called from the Homepage
 */
- (void)testDownloadSpeed
{
    //This is the URL to the file tha I have store in the cloud.
    NSURL *url = [NSURL URLWithString:@"http://whitecoatlabs.co/Mobile%20API/text.rtf"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    _startTime = [NSDate date];
    _length = 0;
    
    //Starting the connection
    _connection = [NSURLConnection connectionWithRequest:request delegate:self];
    double delayInSeconds = kMaximumElapsedTime;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (_connection)
        {
            [_connection cancel];
            _connection = nil;
        }
    });
}

@end
