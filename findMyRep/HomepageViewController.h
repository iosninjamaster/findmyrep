//
//  HomepageViewController.h
//  findMyRep
//
//  Created by Joal Arcos on 4/28/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultsViewController.h"
#import "GetInfoForMembers.h"
#import "Reachability.h"
#import "InternetTesting.h"
#import "HistoryResultsViewController.h"

@interface HomepageViewController : UIViewController <UITextFieldDelegate, RequestDelegate, UIScrollViewDelegate, InternetTestingDelegate, HistoryResultsDelegate>
{
    IBOutlet UITextField *textField_RepName;
    IBOutlet UITextField *textField_RepState;
    IBOutlet UITextField *textField_SenName;
    IBOutlet UITextField *textField_SenState;
    IBOutlet UITextField *textField_zip;
    IBOutlet UIScrollView *mainScrollView;
    IBOutlet UIView *indicatorVIew;
    IBOutlet UIActivityIndicatorView *hud;
    IBOutlet UIImageView *image_ConnectionType;
    IBOutlet UILabel *label_connectionSpeed;
    IBOutlet UIView *speedTestView;
    IBOutlet UILabel *label_ConnectionStatus;
}

- (IBAction)search:(id)sender;
- (IBAction)showHistoryForRepresentatives:(id)sender;
- (IBAction)showHistoryForSenators:(id)sender;
- (IBAction)showHistoryForAllMembers:(id)sender;
@end
