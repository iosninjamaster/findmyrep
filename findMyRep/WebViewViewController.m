//
//  WebViewViewController.m
//  findMyRep
//
//  Created by Joal Arcos on 4/29/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import "WebViewViewController.h"

@interface WebViewViewController ()

@end

@implementation WebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set the URL and load the request
    NSURL *url = [NSURL URLWithString:_selectedURL];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    [mainWebView loadRequest:request];
}




@end
