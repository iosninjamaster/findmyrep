//
//  GetInfoForMembers.h
//  findMyRep
//
//  Created by Joal Arcos on 4/29/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Requests.h"
@class HomepageViewController;

@interface GetInfoForMembers : NSObject

//This is the public method called from the HomePageVC
- (void)getInfoForParam:(NSString *)param withType:(NSString *)type;

@property (nonatomic) HomepageViewController *homepage;
@end
