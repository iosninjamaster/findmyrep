//
//  ResultsViewController.h
//  findMyRep
//
//  Created by Joal Arcos on 4/28/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BioTableViewCell.h"
@interface ResultsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    
IBOutlet UITableView *senatorsTableView;
}
@property (nonatomic) NSDictionary *results;
@end
