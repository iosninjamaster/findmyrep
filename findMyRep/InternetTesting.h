//
//  InternetTesting.h
//  findMyRep
//
//  Created by Joal Arcos on 4/30/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
@protocol InternetTestingDelegate <NSObject>
- (void)updateConnectionSpeed:(float)speed;
- (void)noInternetError:(NSError *)error;
@end

@interface InternetTesting : NSObject <NSURLConnectionDelegate>
@property (nonatomic, assign) id <InternetTestingDelegate> delegate;
@property (nonatomic, assign) Reachability *reachability;
@property (nonatomic, strong) NSURLConnection *connection; // we'll use presence or existence of this connection to determine if download is done
@property (nonatomic) NSUInteger length;                   // the numbers of bytes downloaded from the server thus far
@property (nonatomic, strong) NSDate *startTime;           // when did the download start
- (void)testDownloadSpeed;

@end
