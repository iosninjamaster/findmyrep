//
//  HistoryResultsViewController.h
//  findMyRep
//
//  Created by Joal Arcos on 5/30/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HistoryResultsDelegate <NSObject>
- (void)removeTableView;
@end

@interface HistoryResultsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
IBOutlet UIButton *button_Cancel;
}

@property (nonatomic) id <HistoryResultsDelegate>delegate;
@property (weak, nonatomic) IBOutlet UITableView *resultsTable;


- (IBAction)dismissTableView:(id)sender;
@end
