//
//  GetInfoForMembers.m
//  findMyRep
//
//  Created by Joal Arcos on 4/29/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import "GetInfoForMembers.h"
#import "HomepageViewController.h"
@implementation GetInfoForMembers

- (void)getInfoForParam:(NSString *)param withType:(NSString *)type
{
    //I have set up a data layer (Requests class) that handles executing the connection and receiving response data.
    Requests *request = [Requests new];
    request.delegate = _homepage;
    
    //Passing the values from the Interactor to the data layer.
    [request executePostRequestWithParam:param andType:type];
}
@end
