//
//  ResultsViewController.m
//  findMyRep
//
//  Created by Joal Arcos on 4/28/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import "ResultsViewController.h"

@interface ResultsViewController ()
@property (nonatomic) NSMutableArray *resultsArray;

@end

@implementation ResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setting up the array used to populate the tableView
    _resultsArray = [NSMutableArray new];
    _resultsArray = [_results valueForKey:@"results"];
    
    //Registering the customCell NIB file
    UINib *cellNib = [UINib nibWithNibName:@"BioTableViewCell" bundle:nil];
    [senatorsTableView registerNib:cellNib forCellReuseIdentifier:@"customCell"];
    
    //Disable tableview selection highlighting
    [senatorsTableView setAllowsSelection:NO];
}

#pragma mark Table view data source
/**
 *  Set 1 section
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/**
 *  Set up the number of rows on the table
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _resultsArray.count;
}

/**
 *  Customize the appearance of table view cells.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BioTableViewCell *cell = (BioTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"customCell"];
    if (cell == nil)
    {
        cell = (BioTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"customCell"];
    }
    
    // Configure the cell...
    
    //Send a reference to the navigation controller so that I can push/pop views from the custom cell.
    cell.navi = self.navigationController;
    
    //Set all the cell properties
    cell.name.text = [_resultsArray[indexPath.row] valueForKey:@"name"];
    cell.district.text = [_resultsArray[indexPath.row] valueForKey:@"district"];
    [cell.link setTitle:[_resultsArray[indexPath.row] valueForKey:@"link"] forState:UIControlStateNormal];
    cell.office.text = [_resultsArray[indexPath.row] valueForKey:@"office"];
    cell.party.text = [_resultsArray[indexPath.row] valueForKey:@"party"];
    cell.phone.text = [_resultsArray[indexPath.row] valueForKey:@"phone"];
    cell.state.text = [_resultsArray[indexPath.row] valueForKey:@"state"];
    
    //Set the image on the custom cell
    if ([[_resultsArray[indexPath.row] valueForKey:@"party"] isEqualToString:@"D"])
    {
        cell.partyImage.image = [UIImage imageNamed:@"democrat"];
    }
    else
    {
        cell.partyImage.image = [UIImage imageNamed:@"republican"];
    }
    return cell;
}

#pragma mark Table view delegate

/**
 *  Set the custom cell height to match the customerCell NIB height.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 131;
}

@end
