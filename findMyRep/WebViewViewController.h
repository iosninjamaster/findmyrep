//
//  WebViewViewController.h
//  findMyRep
//
//  Created by Joal Arcos on 4/29/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewViewController : UIViewController
{
    IBOutlet UIWebView *mainWebView;
}
@property (nonatomic) NSString *selectedURL;
@end
