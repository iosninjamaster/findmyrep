//
//  Requests.h
//  findMyRep
//
//  Created by Joal Arcos on 4/29/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
@protocol RequestDelegate <NSObject>
- (void)startHUD;
- (void)stopHUD;
- (void)loadDictioary:(NSDictionary *)resultsDictionary;
@end

@interface Requests : NSObject <NSURLConnectionDelegate>

- (void)executePostRequestWithParam:(NSString *)param andType:(NSString *)type;

@property (nonatomic) NSMutableData *responseData;
@property (nonatomic, assign) id <RequestDelegate> delegate;
@property (nonatomic, assign) Reachability *reachability;

@end
