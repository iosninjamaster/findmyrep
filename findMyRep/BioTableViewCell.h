//
//  BioTableViewCell.h
//  findMyRep
//
//  Created by Joal Arcos on 4/28/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewViewController.h"
#import "HomepageViewController.h"
@interface BioTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *district;
@property (weak, nonatomic) IBOutlet UILabel *party;
@property (weak, nonatomic) IBOutlet UIButton *link;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UITextView *office;
@property (weak, nonatomic) IBOutlet UIImageView *partyImage;
@property (nonatomic) UINavigationController *navi;

- (IBAction)sendLink:(id)sender;
@end
