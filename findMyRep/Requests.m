//
//  Requests.m
//  findMyRep
//
//  Created by Joal Arcos on 4/29/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import "Requests.h"
@interface Requests()

@end

@implementation Requests

/**
 *  This class is called from an interactor and executes the URL request and passes data back via delegates.
 */
- (void)executePostRequestWithParam:(NSString *)param andType:(NSString *)type
{
    //watch for changes in intertet connection
    
    NSString *url;
    if ([type isEqualToString:@"rep_last_name"])
    {
        url = @"getall_reps_byname.php?name";
    }
    else if ([type isEqualToString:@"rep_state"])
    {
        url = @"getall_reps_bystate.php?state";
    }
    else if ([type isEqualToString:@"sen_last_name"])
    {
        url = @"getall_sens_byname.php?name";
    }
    else if ([type isEqualToString:@"sen_state"])
    {
        url = @"getall_sens_bystate.php?state";
    }
    else if ([type isEqualToString:@"zip"])
    {
        url = @"getall_mems.php?zip";
    }

    // Create the request.
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://whoismyrepresentative.com/%@=%@&output=json", url, param]]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [_delegate startHUD];
    [conn start];
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    NSError* error;
    
    //Create an NSDictionary from the response data
    NSDictionary *resultsDictionary = [NSJSONSerialization JSONObjectWithData:_responseData options:kNilOptions error:&error];
    
    //Pass the response dictionary to the Homepage
    [_delegate loadDictioary:resultsDictionary];
    
    //Tell the HUD in the HomePage to stop
    [_delegate stopHUD];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}







@end
