//
//  HomepageViewController.m
//  findMyRep
//
//  Created by Joal Arcos on 4/28/15.
//  Copyright (c) 2015 myOrg. All rights reserved.
//

#import "HomepageViewController.h"
#import "HistoryResultsViewController.h"


@interface HomepageViewController ()
@property (nonatomic) UITextField *lastTextField;
@property (nonatomic) CGRect originalPosition; //Used to move the main view back to it's original position
@property (nonatomic) CGRect wifiImageRect; //The wifi image is the only one that has different aspect ratio. This is used to change the UIImage frame when the image is selected;
@property (nonatomic) HistoryResultsViewController *hrvc;
@end
static float const kMinimumMegabytesPerSecond = .5;//These are used to determine what color to set speedTestView
static float const kMediumMegabytesPerSecond = 1.0;
static float const kHightMegabytesPerSecond = 3.0;

@implementation HomepageViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	_originalPosition = self.view.frame;
	_wifiImageRect = CGRectMake(image_ConnectionType.frame.origin.x, image_ConnectionType.frame.origin.y, 41, 41);
}

/**
 *  This method is used to update the internet connection speed. It is called every 2 seconds.
 */
- (void)testInternetConnection
{
	InternetTesting *testing = [InternetTesting new];
	testing.delegate = self;
	[testing testDownloadSpeed];
}
- (void)viewWillAppear:(BOOL)animated
{
	//NSTimer to update the connection status every 2 seconds
	[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(testInternetConnection) userInfo:nil repeats:YES];
	
	//This class is observing for changes in the internet connection.
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
	
	//Checking the internet status
	Reachability *reachability = [Reachability reachabilityForInternetConnection];
	NetworkStatus internetStatus = [reachability currentReachabilityStatus];
	
	//Setting the connection type image
	if (internetStatus == NotReachable)
	{
		image_ConnectionType.image = [UIImage imageNamed:@"no_signal_image"];
		//change the size of imageView as it is a different ratio
		image_ConnectionType.frame =_wifiImageRect;
	}
	else if (internetStatus == ReachableViaWiFi)
	{
		
		image_ConnectionType.image = [UIImage imageNamed:@"wifi_icon"];
	}
	else if (internetStatus == ReachableViaWWAN)
	{
		image_ConnectionType.image = [UIImage imageNamed:@"cell_icon"];
	}
	
	[self.navigationController setNavigationBarHidden:YES];
	
	//Setting up scrollView
	mainScrollView.contentSize = CGSizeMake(375, 800);
	mainScrollView.scrollEnabled = NO;
	
	//Moving scroll to original position when user lands on page
	[mainScrollView scrollRectToVisible:_originalPosition animated:YES];
	
	//Setting all the textfields to empty strings to avoid having multiple textfields with text.
	textField_RepName.text = @"";
	textField_RepState.text = @"";
	textField_SenName.text = @"";
	textField_SenState.text = @"";
	textField_zip.text = @"";
}

/**
 *  This is a delegate method that gets called when the internet connection status changes.
 */
#pragma mark Reachability Delegate method
- (void)reachabilityDidChange:(NSNotification *)notification
{
	Reachability *reachability = (Reachability *)[notification object];
	NetworkStatus internetStatus = [reachability currentReachabilityStatus];
	
	//Setting the Connection type images
	if (internetStatus == NotReachable)
	{
		[self setupAlertWithStatus:@"No Internet Connection" andMessage:@"There is no internet connection available. Check your device settings."];
		image_ConnectionType.image = [UIImage imageNamed:@"no_signal_image"];
		image_ConnectionType.frame =_wifiImageRect;
	}
	else if (internetStatus == ReachableViaWiFi)
	{
		image_ConnectionType.image = [UIImage imageNamed:@"wifi_icon"];
	}
	else if (internetStatus == ReachableViaWWAN)
	{
		image_ConnectionType.image = [UIImage imageNamed:@"cell_icon"];
	}
}

/**
 *  This is the alert that gets called from multiple places that comes up when there is slow or no internet connection.
 *  Both parameters passed can be customized to display on the alert.
 */
- (void)setupAlertWithStatus:(NSString *)status andMessage:(NSString *)message
{
	UIAlertController *alert = [UIAlertController
								alertControllerWithTitle:status
								message:message
								preferredStyle:UIAlertControllerStyleAlert];
	[self presentViewController:alert animated:YES completion:nil];
	
	UIAlertAction *okAction = [UIAlertAction
							   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
							   style:UIAlertActionStyleDefault
							   handler:^(UIAlertAction *action)
							   {
								   NSLog(@"OK action");
							   }];
	[alert addAction:okAction];
}

/**
 *  This is called when the user taps on the search button. It passes the values set in the textfields to an interactor class which passes info to the data layer.
 */
- (IBAction)search:(id)sender
{
	if (textField_zip.text.length == 0 && textField_RepName.text.length == 0 && textField_SenName.text.length == 0 && textField_SenState.text.length == 0 && textField_RepState.text.length == 0)
	{
		[self setupAlertWithStatus:@"Empty Field" andMessage:@"Textfields cannot be empty"];
	}else
	{
		Reachability *reachability = [Reachability reachabilityForInternetConnection];
		NetworkStatus internetStatus = [reachability currentReachabilityStatus];
		
		//If there is no internet connection an alert is presented and the appropiates images set.
		if (internetStatus == NotReachable)
		{
			[self setupAlertWithStatus:@"No Internet Connection" andMessage:@"There is no internet connection available. Check your device settings."];
			image_ConnectionType.image = [UIImage imageNamed:@"no_signal_image"];
			//change the size of imageView as it is a different ratio
			image_ConnectionType.frame =_wifiImageRect;
		}
		else
		{
			//If there is internet it passes the textfield values to the interactor class (GetInformationForReps).
			GetInfoForMembers *getDataForReps = [GetInfoForMembers new];
			getDataForReps.homepage = self;
			
			if (_lastTextField == textField_RepName)
			{
				[getDataForReps getInfoForParam:textField_RepName.text withType:@"rep_last_name"];
			}
			else if (_lastTextField == textField_RepState)
			{
				[getDataForReps getInfoForParam:textField_RepState.text withType:@"rep_state"];
			}
			else if (_lastTextField == textField_SenName)
			{
				[getDataForReps getInfoForParam:textField_SenName.text withType:@"sen_last_name"];
			}
			else if (_lastTextField == textField_SenState)
			{
				[getDataForReps getInfoForParam:textField_SenState.text withType:@"sen_state"];
			}
			if (_lastTextField == textField_zip)
			{
				[getDataForReps getInfoForParam:textField_zip.text withType:@"zip"];
			}
		}
		// Update images on connection type for either wifi or cellular networks.
		if (internetStatus == ReachableViaWiFi)
		{
			image_ConnectionType.image = [UIImage imageNamed:@"wifi_icon"];
		}
		else if (internetStatus == ReachableViaWWAN)
		{
			image_ConnectionType.image = [UIImage imageNamed:@"cell_icon"];
		}
		
	}
	
}

- (IBAction)showHistoryForRepresentatives:(id)sender
{
	_hrvc = [HistoryResultsViewController new];
	[_hrvc.view setFrame:CGRectMake(_hrvc.view.frame.origin.x, 140, _hrvc.view.frame.size.width, _hrvc.view.frame.size.height)];
	_hrvc.delegate = self;
	[UIView transitionWithView:self.view duration:.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{[self.view addSubview:_hrvc.view];} completion:nil];
}
- (IBAction)showHistoryForSenators:(id)sender
{
	_hrvc = [HistoryResultsViewController new];
	[_hrvc.view setFrame:CGRectMake(_hrvc.view.frame.origin.x, 328, _hrvc.view.frame.size.width, _hrvc.view.frame.size.height)];
	_hrvc.delegate = self;
	[UIView transitionWithView:self.view duration:.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{[self.view addSubview:_hrvc.view];} completion:nil];
	

}
- (IBAction)showHistoryForAllMembers:(id)sender
{
	_hrvc = [HistoryResultsViewController new];
	[_hrvc.view setFrame:CGRectMake(_hrvc.view.frame.origin.x, 514, _hrvc.view.frame.size.width, _hrvc.view.frame.size.height)];
	_hrvc.delegate = self;
	[UIView transitionWithView:self.view duration:.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{[self.view addSubview:_hrvc.view];} completion:nil];
}

#pragma mark TextView Delegate Method
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	//I'm disabling the scrollview on the XIB file to prevent the user from being able to scroll until after they tap on a textfield.
	//This re-enables the scrolling.
	mainScrollView.scrollEnabled = YES;
	
	// This is used to keep a reference to the last textfield that was tapped.
	_lastTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	// This is used to keep a reference to the last textfield that was tapped.
	_lastTextField = textField;
}


#pragma mark Requests Delegate methods
/**
 *  This delegate method is called as soon as the executePostRequestWithParam is started in the interactor (Requests Class).
 */
- (void)startHUD
{
	[self.view addSubview:indicatorVIew];
	[indicatorVIew setBackgroundColor:[UIColor colorWithWhite:0.800 alpha:.50]];
	[hud startAnimating];
}

/**
 *  This delegate method is called as soon as the connectionDidFinishLoading method in the interactor class (Requests Class) is called.
 */
- (void)stopHUD
{
	[hud stopAnimating];
	[indicatorVIew removeFromSuperview];
}

/**
 *  This delegate method is called as soon as the connectionDidFinishLoading method in the interactor class (Requests Class) is called.
 *  It passes in the data from the URL request which is used to populate the tableView on ResultsViewController.
 */
- (void)loadDictioary:(NSDictionary *)resultsDictionary
{
	if (resultsDictionary.count == 0)
	{
		[self setupAlertWithStatus:@"0 matches" andMessage:@"No search resulsts were found."];
	}
	else
	{
		ResultsViewController *viewController = [ResultsViewController new];
		
		//Unhide navigation bar
		[self.navigationController setNavigationBarHidden:NO];
		
		//Sets the scrollView to the original position
		[mainScrollView scrollRectToVisible:_originalPosition animated:YES];
		
		viewController.results = resultsDictionary;
		[self.navigationController pushViewController:viewController animated:YES];
		
		//Makes sure that the keyboard is hidden when the next view is pushed. Otherwise the keyboard is up when you come back to this View.
		[_lastTextField resignFirstResponder];
	}
}

#pragma mark ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	//Hides the keyboard while the user is scrolling around.
	[_lastTextField resignFirstResponder];
}

#pragma mark InternetTesting Delegate methods
/**
 *  This is the method called from the interactor class (Internet Testing). It sends the speed at which the 4KB test files was downloaded.
 */
- (void)updateConnectionSpeed:(float)speed
{
	//Setting Connection status labels and speedTestView color.
	if (speed > 0 && speed < kMinimumMegabytesPerSecond)
	{
		speedTestView.backgroundColor = [UIColor redColor];
		label_ConnectionStatus.text = @"Slow";
	}
	else if (speed > kMediumMegabytesPerSecond && speed <kHightMegabytesPerSecond)
	{
		speedTestView.backgroundColor = [UIColor colorWithRed:0.895 green:0.776 blue:0.062 alpha:1.000];
		label_ConnectionStatus.text = @"Medium";
	}
	else if (speed > kHightMegabytesPerSecond)
	{
		speedTestView.backgroundColor = [UIColor colorWithRed:0.127 green:0.830 blue:0.099 alpha:1.000];
		label_ConnectionStatus.text = @"Fast";
	}
	
	//Setting the ConnectionSpeed label
	label_connectionSpeed.text = [NSString stringWithFormat:@"%.1f", speed];
}

/**
 *  This delegate method is called from the interactor (Internet Testing) when there is no internet connection.
 */
- (void)noInternetError:(NSError *)error
{
	//Setting the connectionStatus, the connectionSpeed label and the speedTestView to show no internet connection speeds.
	speedTestView.backgroundColor = [UIColor blackColor];
	label_ConnectionStatus.text = @"NONE";
	label_connectionSpeed.text = @"0";
}

#pragma mark HistoryResultsVCDelegate methods
- (void)removeTableView
{
	[UIView transitionWithView:self.view duration:.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{[_hrvc.view removeFromSuperview];} completion:nil];

}
@end
